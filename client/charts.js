/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-useless-concat */
/* eslint-disable array-callback-return */
/* eslint-disable dot-notation */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
fetch('../output/data.json').then(res => res.json()).then((json) => {
  olympicHostCity(json.olympicHostedPerCity);
  top10MedalWinnerCountry(json.topMedalWinnerCountry);
  mfDecadeParticipation(json.decade_M_F_Participation);
  boxingAvgAgePerYear(json.avgAgePerYear);
});

function hostedCity(result) {
  return Object.keys(result).reduce((acc, cur) => {
    acc.push({
      name: cur,
      y: result[cur],
    });
    return acc;
  }, []);
}

function olympicHostCity(hostedOlympic) {
  const data = hostedCity(hostedOlympic);
  Highcharts.chart('cityCount', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: 'Olympics Events Hosted Per City',
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.y}</b>',
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y}',
        },
      },
    },
    series: [{
      name: 'Olympic Hosted',
      colorByPoint: true,
      data,
    }],
  });
}

function top10MedalWinnerCountry(mostMedalWinner) {
  const Gold = [];
  const Silver = [];
  const Bronze = [];
  Object.keys(mostMedalWinner).map((country) => {
    Object.keys(mostMedalWinner[country]).map((medal) => {
      if (mostMedalWinner[country][medal].hasOwnProperty('Gold')) {
        Gold.push(mostMedalWinner[country][medal]['Gold']);
      }
      if (mostMedalWinner[country][medal].hasOwnProperty('Silver')) {
        Silver.push(mostMedalWinner[country][medal]['Silver']);
      }
      if (mostMedalWinner[country][medal].hasOwnProperty('Bronze')) {
        Bronze.push(mostMedalWinner[country][medal]['Bronze']);
      }
    }, {});
  }, {});
  const country = [];
  Object.keys(mostMedalWinner).map((cur) => {
    country.push(Object.keys(mostMedalWinner[cur]));
  }, {});
  Highcharts.chart('countryMedalChart', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Top Ten Countries With Most Medals',
    },
    xAxis: {
      categories: country,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total Medals Won',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style
            && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [{
      name: 'Gold',
      data: Gold,
    }, {
      name: 'Silver',
      data: Silver,
    }, {
      name: 'Bronze',
      data: Bronze,
    }],
  });
}

function mfDecadeParticipation(mfParticipation) {
  const Male = [];
  const Female = [];
  const decade = [];
  Object.keys(mfParticipation).map((cur) => {
    Male.push(mfParticipation[cur]['Male']);
    Female.push(mfParticipation[cur]['Female']);
  }, {});
  Object.keys(mfParticipation).map((cur) => {
    decade.push(cur);
    return decade;
  }, {});
  Highcharts.chart('mf_DecadeParticipation', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Male/Female Participation in Olympics Per Decade',
    },
    xAxis: {
      categories: decade,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of Males and Females',
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.medal}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
      + '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    series: [{
      name: 'Male',
      data: Male,
    },
    {
      name: 'Female',
      data: Female,
    },
    ],
  });
}

function boxingAvgAgePerYear(avgAge) {
  const averageAge = [];
  const year = [];
  Object.keys(avgAge).map((age) => {
    averageAge.push(parseFloat(avgAge[age]));
    return averageAge;
  });

  Object.keys(avgAge).map((cur) => {
    year.push(cur);
    return year;
  });
  Highcharts.chart('avgAgePerYear', {
    chart: {
      type: 'line',
    },
    title: {
      text: "Average Age of Men's Boxing Participant",
    },
    subtitle: {
      text: 'Olympic',
    },
    xAxis: {
      categories: year,
    },
    yAxis: {
      title: {
        text: 'Average Age',
      },
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true,
        },
        enableMouseTracking: false,
      },
    },
    series: [{
      name: 'Average',
      data: averageAge,
    }],
  });
}
