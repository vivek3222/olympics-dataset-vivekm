/* eslint-disable max-len */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-else-return */
/* eslint-disable radix */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable dot-notation */


const fs = require('fs');
const csv = require('./csv_to_json');

const athleteData = csv.convertMyFile('../data/athlete_events.csv');
// console.log(athleteData);

function getOlympicsPerCity() {
  const set = new Set();
  const gamesPerCity = {};

  athleteData.forEach((event) => {
    const uniqueKey = `${event.Games} ${event.City}`;
    if (!set.has(uniqueKey)) {
      set.add(uniqueKey);
      // eslint-disable-next-line max-len
      // eslint-disable-next-line no-unused-expressions
      gamesPerCity.hasOwnProperty(event.City) ? ++gamesPerCity[event.City] : gamesPerCity[event.City] = 1;
    }
  });
  return gamesPerCity;
}
// console.log(getOlympicsPerCity());

function maleFemaleParticipationDecade() {
  const mfParticipation = athleteData.reduce((maleFemale, participant) => {
    if (maleFemale.hasOwnProperty(participant['Year'])) {
      if ((!maleFemale[participant['Year']]['Names'].hasOwnProperty(participant['Name'])) && (maleFemale[participant['Year']]['Names'][participant['Name']] !== participant['Season'])) {
        maleFemale[participant['Year']][participant['Sex']]++;
        maleFemale[participant['Year']]['Names'][participant['Name']] = participant['Season'];
      }
    } else {
      maleFemale[participant['Year']] = {};
      maleFemale[participant['Year']]['M'] = 0;
      maleFemale[participant['Year']]['F'] = 0;
      maleFemale[participant['Year']]['Names'] = {};
      maleFemale[participant['Year']]['Names'][participant['Name']] = participant['Season'];
      maleFemale[participant['Year']][participant['Sex']] = 1;
    }

    return maleFemale;
  }, {});
  const decades = {
    '1890-1900': {},
    '1900-1910': {},
    '1910-1920': {},
    '1920-1930': {},
    '1930-1940': {},
    '1940-1950': {},
    '1950-1960': {},
    '1960-1970': {},
    '1970-1980': {},
    '1980-1990': {},
    '1990-2000': {},
    '2000-2010': {},
    '2010-2020': {},
  };
  function decadeParticipation(result) {
    const limit = result.split('-');
    Object.keys(mfParticipation).forEach((mfParticipantPerYear) => {
      // eslint-disable-next-line max-len
      if (parseInt(mfParticipantPerYear) >= parseInt(limit[0]) && parseInt(mfParticipantPerYear) < parseInt(limit[1])) {
        if (decades[result].hasOwnProperty('Male') || decades[result].hasOwnProperty('Female')) {
          decades[result]['Male'] += mfParticipation[mfParticipantPerYear]['M'];
          decades[result]['Female'] += mfParticipation[mfParticipantPerYear]['F'];
        } else {
          decades[result]['Male'] = mfParticipation[mfParticipantPerYear]['M'];
          decades[result]['Female'] = mfParticipation[mfParticipantPerYear]['F'];
        }
      }
    }, {});
  }
  Object.keys(decades).forEach((key) => {
    decadeParticipation(key);
  });
  return decades;
}
// console.log(maleFemaleParticipationDecade(athleteData));
function top10MedalWinnerCountry() {
  const medalsWonByCountry = athleteData.filter((key) => {
    if (key['Year'] > 2000 && key['Medal'] !== 'NA') {
      return key;
    }
  }).reduce((medalsWon, medalData) => {
    if (medalsWon.hasOwnProperty(medalData['NOC'])) {
      medalsWon[medalData['NOC']].count += 1;
      medalsWon[medalData['NOC']][medalData['Medal']] += 1;
    } else {
      medalsWon[medalData['NOC']] = {};
      medalsWon[medalData['NOC']]['Gold'] = 0;
      medalsWon[medalData['NOC']]['Silver'] = 0;
      medalsWon[medalData['NOC']]['Bronze'] = 0;
      medalsWon[medalData['NOC']][medalData['Medal']] = 1;
      medalsWon[medalData['NOC']].count = 1;
    }
    return medalsWon;
  }, {});
  // eslint-disable-next-line max-len
  const nocKeys = Object.keys(medalsWonByCountry).sort((min, max) => medalsWonByCountry[max].count - medalsWonByCountry[min].count, {}).slice(0, 10);
  return nocKeys.map((key) => {
    delete medalsWonByCountry[key].count;
    return {
      [key]: medalsWonByCountry[key],
    };
  });
  // return medals_data
}
// console.log(top10MedalWinnerCountry(athleteData));

function boxingMensHeavyWeight() {
  const avgAgeMensBoxing = athleteData.reduce((acc, data) => {
    if (acc.hasOwnProperty(data['Year'])) {
      if (data['Event'] === "Boxing Men's Heavyweight" && data['Age'] !== 'NA') {
        acc[data['Year']].Age += parseInt(data['Age']);
        acc[data['Year']].Count++;
      }
      return acc;
    } else {
      if (data['Event'] === "Boxing Men's Heavyweight" && data['Age'] !== 'NA') {
        acc[data['Year']] = {};
        acc[data['Year']].Age = parseInt(data['Age']);
        acc[data['Year']].Count = 1;
      }
      return acc;
    }
  }, {});
  Object.keys(avgAgeMensBoxing).map((e) => {
    avgAgeMensBoxing[e] = parseFloat(avgAgeMensBoxing[e].Age / avgAgeMensBoxing[e].Count).toFixed(2);
  }, {});
  return avgAgeMensBoxing;
}
// console.log(boxingMensHeavyWeight(athleteData));

function indiaMedalWinner() {
  const winner = athleteData.reduce((acc, data) => {
    if (acc.hasOwnProperty(data['Season'])) {
      if (data['Team'] === 'India' && data['Medal'] !== 'NA') {
        acc[data['Season']].push(data);
      }
      return acc;
    } else {
      if (data['Team'] === 'India' && data['Medal'] !== 'NA') {
        acc[data['Season']] = [data];
      }
      return acc;
    }
  }, {});
  return winner;
}
// console.log(indiaMedalWinner(athleteData));


function convertToJson() {
  const outputData = {};
  outputData['olympicHostedPerCity'] = getOlympicsPerCity();
  outputData['topMedalWinnerCountry'] = top10MedalWinnerCountry();
  outputData['decade_M_F_Participation'] = maleFemaleParticipationDecade();
  outputData['avgAgePerYear'] = boxingMensHeavyWeight();
  outputData['indianMedalWinner'] = indiaMedalWinner();
  fs.writeFileSync('../result/data.json', JSON.stringify(outputData, null, 2));
}
convertToJson();
