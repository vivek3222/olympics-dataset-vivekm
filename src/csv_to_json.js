/* eslint-disable global-require */

exports.convertMyFile = (filename) => {
  const jsonData = require('csvtojson');
  const data = jsonData.fieldDelimiter(',').formatValueByType().getJsonFromCsv(filename);
  return data;
};
