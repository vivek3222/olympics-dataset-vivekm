const athleteData = require('../data/athlete_events.json');

const fs = require('fs');

function getOlympicsPerCity() {
  let set = new Set();
  let gamesPerCity = {};
  athleteData.forEach(function (event) {
    let uniqueKey = event.Games + " " + event.City;

    if (!set.has(uniqueKey)) {
      set.add(uniqueKey);
      gamesPerCity.hasOwnProperty(event.City) ? ++gamesPerCity[event.City] : gamesPerCity[event.City] = 1;
    }
  });
  return gamesPerCity;
} //console.log(getOlympicsPerCity())


function mf_ParticipationDecade() {
  var mf_participation = athleteData.reduce((all_parts, each_part) => {
    if (all_parts.hasOwnProperty(each_part['Year'])) {
      if (!all_parts[each_part['Year']]['Names'].hasOwnProperty(each_part['Name']) && all_parts[each_part['Year']]['Names'][each_part['Name']] != each_part['Season']) {
        all_parts[each_part['Year']][each_part['Sex']]++;
        all_parts[each_part['Year']]['Names'][each_part['Name']] = each_part['Season'];
      }
    } else {
      all_parts[each_part['Year']] = {};
      all_parts[each_part['Year']]['M'] = 0;
      all_parts[each_part['Year']]['F'] = 0;
      all_parts[each_part['Year']]['Names'] = {};
      all_parts[each_part['Year']]['Names'][each_part['Name']] = each_part['Season'];
      all_parts[each_part['Year']][each_part['Sex']] = 1;
    }

    return all_parts;
  }, {});
  var decades = {
    "1890-1900": {},
    "1900-1910": {},
    "1910-1920": {},
    "1920-1930": {},
    "1930-1940": {},
    "1940-1950": {},
    "1950-1960": {},
    "1960-1970": {},
    "1970-1980": {},
    "1980-1990": {},
    "1990-2000": {},
    "2000-2010": {},
    "2010-2020": {}
  };
  Object.keys(decades).forEach(key => {
    decade_participation(key);
  });

  function decade_participation(decade) {
    var limit = decade.split('-');
    Object.keys(mf_participation).forEach(each_year => {
      if (parseInt(each_year) >= parseInt(limit[0]) && parseInt(each_year) < parseInt(limit[1])) {
        if (decades[decade].hasOwnProperty('Male') || decades[decade].hasOwnProperty('Female')) {
          decades[decade]['Male'] += mf_participation[each_year]['M'];
          decades[decade]['Female'] += mf_participation[each_year]['F'];
        } else {
          decades[decade]['Male'] = mf_participation[each_year]['M'];
          decades[decade]['Female'] = mf_participation[each_year]['F'];
        }
      }
    }, {});
  }

  return decades;
} //console.log(mf_participation_decade(athleteData))


function top_10_MedalWinnerCountry() {
  var medals_data = athleteData.filter(key => {
    if (key['Year'] > 2000 && key['Medal'] != 'NA') {
      return key;
    }
  }).reduce((total_medals, each_medal) => {
    if (total_medals.hasOwnProperty(each_medal['NOC'])) {
      total_medals[each_medal['NOC']].count += 1;
      total_medals[each_medal['NOC']][each_medal['Medal']] += 1;
    } else {
      total_medals[each_medal['NOC']] = {};
      total_medals[each_medal['NOC']]['Gold'] = 0;
      total_medals[each_medal['NOC']]['Silver'] = 0;
      total_medals[each_medal['NOC']]['Bronze'] = 0;
      total_medals[each_medal['NOC']][each_medal['Medal']] = 1;
      total_medals[each_medal['NOC']].count = 1;
    }

    return total_medals;
  }, {});
  const noc_keys = Object.keys(medals_data).sort((min, max) => {
    return medals_data[max].count - medals_data[min].count;
  }, {}).slice(0, 10);
  return noc_keys.map(key => {
    delete medals_data[key].count;
    return {
      [key]: medals_data[key]
    };
  }); //return medals_data
} //console.log(top_10_MedalWinnerCountry(athleteData))


function boxingMensHeavyWeight() {
  const avg_MensBoxingPerYear = athleteData.reduce((acc, data) => {
    if (acc.hasOwnProperty(data['Year'])) {
      if (data['Event'] == "Boxing Men's Heavyweight" && data['Age'] != 'NA') {
        acc[data['Year']].Age += parseInt(data['Age']);
        acc[data['Year']].Count++;
      }

      return acc;
    } else {
      if (data['Event'] == "Boxing Men's Heavyweight" && data['Age'] != 'NA') {
        acc[data['Year']] = {};
        acc[data['Year']].Age = parseInt(data['Age']);
        acc[data['Year']].Count = 1;
      }

      return acc;
    }
  }, {});
  Object.keys(avg_MensBoxingPerYear).map(e => {
    avg_MensBoxingPerYear[e] = parseFloat(avg_MensBoxingPerYear[e].Age / avg_MensBoxingPerYear[e].Count).toFixed(2);
  }, {});
  return avg_MensBoxingPerYear;
} //console.log(boxingMensHeavyWeight(athleteData));


function indiaMedalWinner() {
  let winner = athleteData.reduce((acc, data) => {
    if (acc.hasOwnProperty(data["Season"])) {
      if (data['Team'] == "India" && data['Medal'] != "NA") {
        acc[data['Season']].push(data);
      }

      return acc;
    } else {
      if (data['Team'] == "India" && data['Medal'] != "NA") {
        acc[data['Season']] = [data];
      }

      return acc;
    }
  }, {});
  return winner;
} //console.log(indiaMedalWinner(athleteData))


function convertToJson() {
  let completeQueriesData = {};
  completeQueriesData['olympicHostedPerCity'] = getOlympicsPerCity();
  completeQueriesData['topMedalWinnerCountry'] = top_10_MedalWinnerCountry();
  completeQueriesData['decade_M_F_Participation'] = mf_ParticipationDecade();
  completeQueriesData['avgAgePerYear'] = boxingMensHeavyWeight();
  completeQueriesData['indianMedalWinner'] = indiaMedalWinner();
  fs.writeFileSync('../output/data.json', JSON.stringify(completeQueriesData, null, 2));
}

convertToJson();